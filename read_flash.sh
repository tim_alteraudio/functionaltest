#!/bin/sh
openocd -f interface/stlink-v2.cfg -f target/stm32f4x.cfg -c init -c "reset halt" -c "flash read_bank 0 firmware.bin 0 0x100000" -c "reset" -c shutdown
