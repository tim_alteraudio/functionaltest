#!/bin/bash

#!/usr/bin/env bash
cd "$(dirname "$0")"

firmware="firmware.bin"
start_barcode="START."

scan() {
read -p "" response
read -p "" flush
}

promptclose() {
echo "- Properly place PCB in fixture and make sure PCB switch is in \"on\" position."
echo "- Close fixture and press scan start barcode on testrig to continue."
scan
    while test "$response" != "$start_barcode"
    do
    echo "Please scan start barcode only."
    scan
    done
}

scanserial() {
scan
	while test "$response" == "$start_barcode"
	do
	echo "Please scan a serial number barcode."
	scan
	done
	serial=$response
}

running() {
echo " _______  _____  _____  ____  _____  ____  _____  _____  ____  _____   ______"
echo "|_   __ \|_   _||_   _||_   \|_   _||_   \|_   _||_   _||_   \|_   _|.' ___  |"
echo "  | |__) | | |    | |    |   \ | |    |   \ | |    | |    |   \ | | / .'   \_|"
echo "  |  __ /  | '    ' |    | |\ \| |    | |\ \| |    | |    | |\ \| | | |   ____"
echo " _| |  \ \_ \ \__/ /    _| |_\   |_  _| |_\   |_  _| |_  _| |_\   |_\ \`.___]  |"
echo "|____| |___| \`.__.'    |_____|\____||_____|\____||_____||_____|\____|\`._____.'"
}

pass() {
echo " _______         _          ______       ______       _"
echo "|_   __ \       / \       .' ____ \    .' ____ \     | |"
echo "  | |__) |     / _ \      | (___ \_|   | (___ \_|    | |"
echo "  |  ___/     / ___ \      _.____\`.     _.____\`.     | |"
echo " _| |_      _/ /   \ \_   | \____) |   | \____) |    |_|"
echo "|_____|    |____| |____|   \______.'    \______.'    (_)"
}
fail() {
echo " ________        _         _____     _____        _"
echo "|_   __  |      / \       |_   _|   |_   _|      | |"
echo "  | |_ \_|     / _ \        | |       | |        | |"
echo "  |  _|       / ___ \       | |       | |   _    | |"
echo " _| |_      _/ /   \ \_    _| |_     _| |__/ |   |_|"
echo "|_____|    |____| |____|  |_____|   |________|   (_)"
}
error() {
echo " ________  _______     _______      ___   _______     _"
echo "|_   __  ||_   __ \   |_   __ \   .'   \`.|_   __ \   | |"
echo "  | |_ \_|  | |__) |    | |__) | /  .-.  \ | |__) |  | |"
echo "  |  _| _   |  __ /     |  __ /  | |   | | |  __ /   | |"
echo " _| |__/ | _| |  \ \_  _| |  \ \_\  \`-'  /_| |  \ \_ |_|"
echo "|________||____| |___||____| |___|\`.___.'|____| |___|(_)"
}


#running
#pass
#fail
#error

clear
echo
echo "Welcome to the Timetosser functional test rig! How are you today?"
echo

scantext_newpcb="Please scan serial number barcode of next PCB."
scantext_retry_flash="Flashing failed! Is PCB switched on? Re-scan serial number barcode to retry."
scantext=$scantext_newpcb

#when the user has encountered an error with the leds and has scanned the next qr code instead of pressing enter
#we can skip the serial scanning promt, as we already have the next serial stored in 'serial'
skipscan=0

while true
do

# display scanning instruction text and read serial?
if [[ "$skipscan" -eq 0 ]]; then
    echo "****************************************************************************"
    echo
    echo $scantext
    scanserial
fi

#clear skipscan for next round
skipscan=0
clear
echo "Serial number is \"$serial\"";
promptclose

clear
running
echo
real_fimware_name=`readlink -f $firmware`
echo "Flashing $real_fimware_name..."

#make sure the testresults directory is there!
mkdir -p testresults

flash_output="./testresults/${serial}_flash.txt"
./flash.sh $firmware >> $flash_output 2>&1
if [ $? -ne 0 ]; then
    clear
    cat ${flash_output}
    echo
    error
    echo
    scantext=$scantext_retry_flash
    continue
fi

#read response. If it is non-empty, it means user has scanned the next pcb. save 'responce' in 'serial',
#skip the next scan and continue to next loop iteration
echo "Are all red, green and blue leds working?"
echo "Yes: Scan start barcode to continue testing."
echo "No:  Scan serial number  barcode of next PCB to start over."
scan
if test "$response" != "$start_barcode"; then
    serial=$response
    skipscan=1
    continue
fi

echo "Programing serial number \"$serial\" and running tests..."
test_output="./testresults/${serial}_test.txt"
rm ${test_output} 2> /dev/null
(python3 ./test.py ${serial}) >> ${test_output} 2>&1
test_result=$?

#pass
if [ $test_result -eq 0 ]; then
    clear
    pass
    echo
    scantext=$scantext_newpcb
        
# test fail
elif [ $test_result -eq 2 ]; then
    clear
    cat ${test_output}
    echo
    fail
    echo
    scantext=$scantext_newpcb
    
# no serial
elif [ $test_result -eq 3 ]; then
    clear
    cat ${test_output}
    echo
    error
    echo
    scantext="Incorrect serial specified, open fixture and re-scan barcode to retry."

# timeout
elif [ $test_result -eq 4 ]; then
    clear
    cat ${test_output}
    echo
    error
    echo
    scantext="Timeout getting response from PCB, open fixture and re-scan barcode to retry."
    
#non-recoverable error
else
    clear
    cat ${test_output}
    echo
    error
    echo
    echo "Something is wrong with the fixture, please reboot raspberry pi."
    exit 1
fi
done
