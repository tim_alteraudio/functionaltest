#!/bin/sh
if [ -z "$1" ]
then 
    echo "firmware path not specified."
    exit 1
fi

openocd=$(which openocd)
if [ -z "$openocd" ]
then
    echo "openocd doesn't seem to be installed."
    exit 1
fi
$openocd -s /usr/share/openocd/scripts -f ./openocd.cfg -c "program $1 verify reset exit 0x08000000"
